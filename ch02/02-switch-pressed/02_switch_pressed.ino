#include <Arduino.h>
#include "User_Setup.h"

#define SWITCH_PIN 8
#define LED_PIN 4
int switchState = LOW;
int ledState = LOW;

void setup() {
    pinMode(SWITCH_PIN, INPUT);
    pinMode(LED_PIN, OUTPUT);
}

void loop() {
    int reading = digitalRead(SWITCH_PIN);
    if (reading != switchState) {
        if (reading == HIGH && switchState == LOW) ledState = !ledState;
        digitalWrite(LED_PIN, ledState);
        switchState = reading;
    }
}
