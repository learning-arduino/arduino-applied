#include <Arduino.h>
#include "User_Setup.h"

#define SWITCH_PIN 8
#define LED_PIN 4

void setup() {
    pinMode(SWITCH_PIN, INPUT);
    pinMode(LED_PIN, OUTPUT);
}

void loop() {
    int reading = digitalRead(SWITCH_PIN);
    digitalWrite(LED_PIN, reading);
}
